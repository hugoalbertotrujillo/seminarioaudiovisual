const audio = document.querySelector(".audio");
const btnAudio = document.querySelector(".btn-audio");
const musica = document.querySelector(".musica");
let sonido = false

btnAudio.addEventListener("click", () => {
    
    if(!sonido){
        audio.play();
        musica.play();
        let num = parseInt(Math.random()*1000);
        musica.volume = 1;
        console.log(num);
        musica.currentTime = num;
        sonido = true;
    } else {
        audio.pause();
        sonido = false;
        audio.currentTime = 0;
    }
    console.log(sonido);
    // console.log(parseInt(Math.random()*100));
})